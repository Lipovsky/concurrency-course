#pragma once

#include "body.hpp"
#include "stack.hpp"

#include <twist/ed/sure/context.hpp>

namespace exe::fiber {

class Coroutine {
 public:
  explicit Coroutine(Body);

  void Resume();
  void Suspend();

  bool IsDone() const;

 private:
  // ???
};

}  // namespace exe::fiber
