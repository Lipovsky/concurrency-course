#pragma once

#include <exe/runtime/thread_pool.hpp>

namespace exe::fiber {

using Scheduler = runtime::ThreadPool;

}  // namespace exe::fiber
