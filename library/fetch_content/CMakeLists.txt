include(FetchContent)

# --------------------------------------------------------------------

# Offline mode (uncomment next line to enable)
# set(FETCHCONTENT_FULLY_DISCONNECTED ON)

# set(FETCHCONTENT_QUIET OFF)

# --------------------------------------------------------------------

# Libraries

# --------------------------------------------------------------------

# fmt with println

project_log("FetchContent: fmt")

FetchContent_Declare(
        fmt
        GIT_REPOSITORY https://github.com/fmtlib/fmt.git
        GIT_TAG 10.2.1
)
FetchContent_MakeAvailable(fmt)

# --------------------------------------------------------------------

# Unique Function

project_log("FetchContent: function2")

FetchContent_Declare(
        function2
        GIT_REPOSITORY https://github.com/Naios/function2.git
        GIT_TAG 4.2.2
)
FetchContent_MakeAvailable(function2)

# --------------------------------------------------------------------

project_log("FetchContent: wheels")

FetchContent_Declare(
        wheels
        GIT_REPOSITORY https://gitlab.com/Lipovsky/wheels.git
        GIT_TAG 2aa145331ba280796cbb6d5f8ee47b8f06d96642
)
FetchContent_MakeAvailable(wheels)

# --------------------------------------------------------------------

project_log("FetchContent: sure-stack")

FetchContent_Declare(
        sure-stack
        GIT_REPOSITORY https://gitlab.com/Lipovsky/sure-stack.git
        GIT_TAG d05110dadb5e15e8fc436a59bd3a8ed99fee6748
)
FetchContent_MakeAvailable(sure-stack)

# --------------------------------------------------------------------

project_log("FetchContent: sure")

FetchContent_Declare(
        sure
        GIT_REPOSITORY https://gitlab.com/Lipovsky/sure.git
        GIT_TAG c1bb7afc5db4346a3a3b6bd57cecff473dcb2cb6
)
FetchContent_MakeAvailable(sure)

# --------------------------------------------------------------------

project_log("FetchContent: futex_like")

FetchContent_Declare(
        futex_like
        GIT_REPOSITORY https://gitlab.com/Lipovsky/futex_like.git
        GIT_TAG c7953b32f3138277e71f1d001204974ea055a818
)
FetchContent_MakeAvailable(futex_like)

# --------------------------------------------------------------------

project_log("FetchContent: spin_wait")

FetchContent_Declare(
        spin_wait
        GIT_REPOSITORY https://gitlab.com/Lipovsky/spin_wait.git
        GIT_TAG 1c558bec396264c031213c37a48bca0e9dca16c5
)
FetchContent_MakeAvailable(spin_wait)

# --------------------------------------------------------------------

project_log("FetchContent: twist")

FetchContent_Declare(
        twist
        GIT_REPOSITORY https://gitlab.com/Lipovsky/twist.git
        GIT_TAG d879d2acb1fdd37da57dd5446534e6cc6e5cd21d
)
FetchContent_MakeAvailable(twist)

# --------------------------------------------------------------------

project_log("FetchContent: tinyfiber")

FetchContent_Declare(
        tinyfiber
        GIT_REPOSITORY https://gitlab.com/Lipovsky/tinyfiber.git
        GIT_TAG cc8ba1e98780685599d86869cfc7d93bc0b83c26
)
FetchContent_MakeAvailable(tinyfiber)

# --------------------------------------------------------------------

project_log("FetchContent: asio")

FetchContent_Declare(
        asio
        GIT_REPOSITORY https://github.com/chriskohlhoff/asio.git
        GIT_TAG asio-1-29-0
)
FetchContent_MakeAvailable(asio)

add_library(asio INTERFACE)
target_include_directories(asio INTERFACE ${asio_SOURCE_DIR}/asio/include)

# --------------------------------------------------------------------

# Memory allocation

if((CMAKE_BUILD_TYPE MATCHES Release) AND NOT TWIST_FAULTY)
    project_log("FetchContent: mimalloc")

    FetchContent_Declare(
            mimalloc
            GIT_REPOSITORY https://github.com/microsoft/mimalloc
            GIT_TAG master
    )
    FetchContent_MakeAvailable(mimalloc)

endif()
